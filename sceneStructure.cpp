#include "sceneStructure.h"
#include <vector>
#include <iostream>


Point Point::operator-(const Point& p)
{
	Point poi( this-> x - p.x, this-> y - p.y, this-> z - p.z );
	return poi;
}
Point Point::operator+(const Point& p)
{
	Point poi( this-> x + p.x, this-> y + p.y, this-> z + p.z );
	return poi;
}
/*
Vector Point::minusAsVector(Point* p)
{
	Vector v( 	this-> x - p-> x, \
				this-> y - p-> y, \
				this-> z - p-> z);
	return v;
}
*/

Vector* Vector::cross(Vector* v )
{
	double a,b,c,d,e,f, xx,yy,zz;
	a = this-> x;
	b = this-> y;
	c = this-> z;
	d = v-> x;
	e = v-> y;
	f = v-> z;
	xx = b*f - c*e ;
	yy = c*d - a*f ;
	zz = a*e - b*d ;
	Vector * _v = new Vector(xx,yy,zz);
	return _v;
}
double Vector::dot( Vector* v )
{
	return (this->x)*(v->x) + (this->y)*(v->y) + (this->z)*(v->z) ;
}

bool Vector::isUnit()
{
	return ( (this->x)+(this->y)+(this->z) == 1.0 );
}
void Vector::unitify()
{
	double sum = (this->x)+(this->y)+(this->z);
	this-> x /= sum;
	this-> y /= sum;
	this-> z /= sum;
}
Vector Vector::multiply(double d)
{
	Vector v( x*d , y*d, z*d );
	return v;
}
void Vector::negate()
{
	this-> x *= -1;
	this-> y *= -1;
	this-> z *= -1;
}
Point Vector::multiplyAsPoint(double m)
{
	Point p( x*m, y*m , z*m );
	return p;
}
Vector Vector::operator-(const Vector& v)
{
	Vector ret( this-> x - v.x , \
				this-> y - v.y , \
				this-> z - v.z );
	return ret;
}
Vector Vector::operator+(const Vector& v)
{
	Vector ret( this-> x + v.x , \
				this-> y + v.y , \
				this-> z + v.z );
	return ret;
}
void Vector::printVector()
{
	cout<<x<<", "<<y<<", "<<z<<endl;
}



Light operator-(const Light& l;
{
	Light l2 ( r - l.r , g - l.g , b - l.b );
	return l2;
}

Light operator+(const Light& l)
{
	Light l2 ( r + l.r , g + l.g , b + l.b );
	return l2;
}

LightSource:: ~LightSource()
{
	if (this->position)
		delete this->position;
	this->position=0;
	if (this->intensity)
		delete this->intensity;
	this->intensity=0;
}

Material::~Material()
{
	if (this->ambient)
		delete this->ambient;
	this-> ambient = 0;
	if (this->diffuse)
		delete this->diffuse;
	this-> diffuse = 0;
	if (this->specular)
		delete this->specular;
	this-> specular = 0;
	if (this->reflectance)
		delete this->reflectance;
	this-> reflectance = 0;
}

Vector Sphere::getNormal(Point* p)
{
	Point p2( *p - *center );
	Vector v( p2 );
	v.multiply( 1/_radius );
	return v;
}

bool Sphere::intersect(Ray& ray, Hit*& h)
{
	double discrim, aa, bb, cc;
	Point tmp = ray.origin - *(this->center);
	Vector ec( &tmp );
	aa = ray.direction->dot(ray.direction);// $NOFORGET: optimize d.dot(d) by saving into ray
	bb = 2*( ray.direction-> dot(&(ec)) );
	cc = ec.dot(&ec) - _radius*_radius;		// $NOFORGET: optimize
	
	discrim = bb*bb - 4*aa*cc;

	if( discrim < 0 )
		return false;
	
	//else there is a hit!
	h = new Hit();
	
	double t1, t2;
	t1 = (-bb-sqrt(discrim))/(2*aa);
	t2 = (-bb+sqrt(discrim))/(2*aa);

	if ( discrim == 0 )
		h->t = t1;
	else
	{
		if( t1 < t2 )
			h-> t = t1;
		else
			h-> t = t2;
	}
	Point p1(ray.origin);
	Vector v(ray.direction);
	Point p2( v.multiplyAsPoint(h->t) );
	h-> p = new Point(p1+p2);
/*
cout<<"intsct@";
Vector v1(h->p);
v1.printVector();
*/
	return true;
}
Sphere::~Sphere()
{
	//if( this-> material )
	//	delete this-> material;
	//this-> material = 0;
	
	//if( this-> center )
	//	delete this-> center;
	//this-> center = 0;
}


Vector Triangle::getNormal()
{
	Vector v(this->normal);
	return v;
}


bool Triangle::checkSamePlane(Ray& ray, Hit*& h)
{
	if ( !(this->normal) ) // then compute normal
	{
		Point tmp = (*(this->b)) - (*(this-> a));
		Vector v1( tmp );
		tmp =(*(this-> c)) - (*(this-> a));
		Vector *v2 = new Vector( tmp );
		Vector* nor = v1.cross(v2);
		nor->unitify();
		this-> normal = nor;
		delete v2;
	}
	
	
	double nDotd = this->normal->dot(ray.direction);

	if (!nDotd)
		return false;
	
	// else ---> it can be inside or outside of triangle
	// find the intersection point put it in hit obj
	Point tmp = ray.origin - *(this->a);
	Vector origMinusA( tmp );
	double newt = origMinusA.dot((this-> normal)) / (-nDotd) ;
	
	h = new Hit();
	
	h->t = newt;
	// calculate the intersection point
	Point tmpP1( ray.origin );
	Point tmpP2 = ray.direction-> multiplyAsPoint(newt);
	h-> p = new Point( tmpP1 + tmpP2 );
//cout<< "@@@ triangle:CheckPlane hit plane at"<<h->p->x<<","<<h->p->y<<","<<h->p->z<<" with t:"<<h->t<<endl;
	return true;
}
bool Triangle::checkPointInside(Ray& ray, Hit*& h)	// $DANGER: pointer'lar null ise segmentationF!
{
	bool ret=false;

	Point *tmp = new Point(*(h->p) - *(this->a));
	Vector *ap = new Vector( tmp );
	delete tmp;
	tmp = new Point(*(h->p) - *(this->b));
	Vector *bp = new Vector( tmp );
	delete tmp;
	tmp = new Point(*(h->p) - *(this->c));
	Vector *cp = new Vector( tmp );
	delete tmp;
	
	Vector *v1 = this->ab->cross(ap);  //NOFORGET: for-future -- v1= funcReturnsVctor()  vs v1(funcReturnsVctr()) 
	Vector *v2 = this->bc->cross(bp);
	Vector *v3 = this->ca->cross(cp);
	
	v1->unitify();
	v2->unitify();
	v3->unitify();
/*
cout<<"-----------"<<endl;
cout<<"vecs:"<<endl;
v1->printVector();
v2->printVector();
v3->printVector();
cout<<"dots:: v1.v2 = "<<v1->dot(v2)<<" v2.v3 = "<<v2->dot(v3)<<" v3.v1 = "<<v3->dot(v1)<<endl;
*/
	if ( v1->dot(v2) > 0 && v2->dot(v3) > 0 && v3->dot(v1) > 0 )
		ret = true;
	else
	{
		delete h;
		h=0;
		ret = false;
	}

	delete ap;
	delete bp;
	delete cp;
	delete v1;
	delete v2;
	delete v3;

	return ret;
}
bool Triangle::intersect(Ray& ray, Hit*& h)
{
	if ( checkSamePlane(ray, h) )
		return checkPointInside(ray, h);
	return false;
}

Triangle::Triangle(int m, int lst[], int id)
{
	this-> _matID = m;
	for(int i=0; i<3; ++i)	
		_vertices[i] = lst[i];
	
	this-> _id = id;
	this-> a = 0;
	this-> b = 0;
	this-> c = 0;
	this-> normal= 0;
	this-> ab= 0;
	this-> bc= 0;
	this-> ca= 0;
}
Triangle::~Triangle()
{
	
	//if (this-> material)
	//	delete this-> material;
	//this-> material = 0;
	
	//if (this->a)
	//	delete this->a;
	//this-> a = 0;
	//if (this->b)
	//	delete this->b;
	//this-> b = 0;
	//if (this->c)
	//	delete this->c;
	//this-> c = 0;
	
	if( this-> normal )
		delete this-> normal;
	if( this-> ab)
		delete this-> ab;
	if( this-> bc)
		delete this-> bc;
	if( this-> ca)
		delete this-> ca;
	
}


Scene::~Scene()
{
cout<<"=========== end of scene ==========="<<endl;
	if( tempScene )
		delete tempScene;
	
cout<<"lightSourceCount: "<<lightSourceList.size()<<endl;
	for(vector<LightSource*>::iterator it = lightSourceList.begin(); it != lightSourceList.end(); ++it)
		delete *it;
	
	delete cameraInScene;
	
	for(int i=0; i<triangleCount ; ++i)
		delete triangleInScene[i];
	
	for(int i=0; i<sphereCount ; ++i)
		delete sphereInScene[i];
	
cout<<"triangle size: "<<triangleCount<<", sphere size: "<<sphereCount<<endl;
	if( triangleInScene )
		delete triangleInScene;
	if( sphereInScene )
		delete sphereInScene;
	
cout<<"vertexCount: "<<vertexList.size()<<endl;
	for(vector<Vertex*>::iterator it = vertexList.begin(); it != vertexList.end(); ++it)
		delete *it;
cout<<"matCount: "<<materialList.size()<<endl;
	for(vector<Material*>::iterator it = materialList.begin(); it != materialList.end(); ++it)
		delete *it;
}

