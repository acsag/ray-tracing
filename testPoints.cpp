#include <iostream>

#include "sceneStructure.h"

using std::cout;
using std::endl;

void printPt(Point& v)
{
	cout<<v.x<<", "<<v.y<<", "<<v.z<<endl;
}

int main(int argc, char* argv[])
{
	Point p1;
	//printPt( p1 );
	
	Point p2( 1.1,2.2,3.3 );
	printPt( p2 );
	
	Point p3( 8, 9, 10 );
	printPt( p3 );

	Point p4( p3 - p2 );
	printPt( p4 );
	
	Point p5 = p4 + p2
	printPt( p5 );

	p1 = p5
	Point p6 = p5

	return 0;
}
