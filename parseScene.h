#ifndef __parseScene_h__
#define __parseScene_h__
#include <vector>
#include "sceneStructure.h"

bool parseSceneXML(const char* filename, Scene* scene);

#endif //__parseScene_h__
