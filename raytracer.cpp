#include <iostream>
#include "raytracer.h"
#include <limits>



namespace RYTRCR
{

	using std::vector;
	
	void prepareScene(Scene& scene)
	{
		/*
		 *	PURPOSE: 
		 *				prepares scene and data structure for ray tracing
		 *			and de deallocates scene.tempScene
		 *
		 */
		
		int aa,bb,cc, i;
		TempScene* tscene = scene.tempScene;
		Scene* sscene = &scene;

		scene.triangleCount = (tscene-> triangleList).size();
		scene.triangleInScene = new Triangle* [scene.triangleCount];

		i=0;
		for(vector<Triangle*>::iterator trii = tscene-> triangleList.begin(); trii != tscene-> triangleList.end(); ++trii)
		{
			Triangle* tri = *trii;

			aa = tri->_vertices[0];
			bb = tri->_vertices[1];
			cc = tri->_vertices[2];

			for(vector<Vertex*>::iterator itt = sscene-> vertexList.begin(); itt != sscene-> vertexList.end(); ++itt)
			{
				Vertex* it = *itt;
				if( it->_vertexID ==  aa )
					tri-> a = it->_p ;
				if( it->_vertexID ==  bb )
					tri-> b = it->_p ;
				if( it->_vertexID ==  cc )
					tri-> c = it->_p ;
			}
			
			aa = tri->_matID;
			for(vector<Material*>::iterator it = sscene-> materialList.begin(); it != sscene-> materialList.end(); ++it)
			{
				if( (*it)->matID == aa )
				{
					tri-> material = *it;
					break;
				}
			}
			scene.triangleInScene[i++] = tri;

			Point tmp = (*(tri->b)) - (*(tri-> a));
			tri-> ab = new Vector( tmp );
			
			tmp =(*(tri-> c)) - (*(tri-> a));
			Vector *v2 = new Vector( tmp );
			Vector* nor = (tri-> ab)->cross(v2);
			nor->unitify();
			tri-> normal = nor;
cout<<"___normal: ";
nor->printVector();
			v2->negate();
			tri-> ca = v2;
			
			tmp = *(tri-> c) - *(tri-> b);
			tri-> bc = new Vector(tmp);
cout<<"__ab: ";
tri->ab->printVector();
cout<<"__bc: ";
tri->bc->printVector();
cout<<"__ca: ";
tri->ca->printVector();

		}
		
		scene.sphereCount = (tscene-> sphereList).size();
		scene.sphereInScene = new Sphere* [scene.sphereCount];
		i=0;
		for(vector<Sphere*>::iterator spp = tscene-> sphereList.begin(); spp != tscene-> sphereList.end(); ++spp)
		{
			Sphere* sp = *spp;

			aa = sp->_vertex;

			for(vector<Vertex*>::iterator itt = sscene-> vertexList.begin(); itt != sscene-> vertexList.end(); ++itt)
			{
				Vertex* it = *itt;
				if( it->_vertexID ==  aa )
					sp-> center = it->_p ;
			}
			
			aa = sp->_matID;
			for(vector<Material*>::iterator it = sscene-> materialList.begin(); it != sscene-> materialList.end(); ++it)
			{
				if( (*it)->matID == aa )
				{
					sp-> material = *it;
					break;
				}
			}

			scene.sphereInScene[i++] = sp;
			
		}
	}

	
	Hit* checkIntersection(Ray& ray, Scene& sscene)
	{
		/*
		 *		PURPOSE:
		 *					Gets ray and checks intersections with all objects
		 *				returns closest Hit or null
		 */
		Hit *h=0, *retHit=0;
		TempScene* tscene=sscene.tempScene;
		double tmin = std::numeric_limits<double>::max(); //1000000.0;
		Triangle** triangles=0;
		Sphere** spheres=0;

		triangles = sscene.triangleInScene;
		int end = sscene.triangleCount;
		//ray.direction->unitify();
//cout<<"raydir: ";
//ray.direction->printVector();
		for(int i=0; i<end; ++i)
		{
//cout<<"@@@ checkIntersection() ==i/end: "<<i<<"/"<<end<<endl;
			if( triangles[i]->intersect(ray, h) )
			{
cout<<"****new-t: "<<h->t<<" vs "<<tmin<<endl;
				if( (h-> t < tmin) && (h-> t > 1.0) )
				{
cout<<"yep"<<endl;
					tmin = h->t;
					if( retHit )
						delete retHit;
					retHit = h;
				}
				else
					delete h;
				h = 0;
			}
		}
		
		spheres = sscene.sphereInScene;
		end = sscene.sphereCount;
		for(int i=0; i<end; ++i)
		{
			if( spheres[i]->intersect(ray, h) )
			{
				if( (h-> t) < tmin && (h-> t) > 1 )
				{
					if( retHit )
						delete retHit;
					retHit = h;
				}
				else
					delete h;
				h = 0;
			}
		}
		return retHit;
		/*
		for( vector<Triangle*>::iterator it = tscene-> triangleList.begin(); it != tscene-> triangleList.end() ; ++it )
		{
			if( (*it)-> intersect(h) )
			{
				if( h->t < tmin && h->t > 1 )
				{
					tmin = h->t;
					p = *(h->p);
				}
				delete h;
				h = 0;
			}
		}
		for( vector<Sphere*>::iterator it = tscene-> sphereList.begin(); it != tscene-> sphereList.end() ; ++it )
		{
			if( (*it)-> intersect(h) )
			{
				if( h->t < tmin && h->t > 1 )
				{
					tmin = h->t;
					p = *(h->p);
				}
			}
		}
		*/
	}
	
	Light shading(Hit& h, Scene& scene)
	{
		Light* ld = shadingDiffuse(h, scene);
		Light* ls = shadingSpecular(h, scene);
		Light* la = shadingAmbient(scene);
		
		//TODO: next=> multiple point lights
		return ( ld + ls + la ); 
	}



	void traceRays(Scene& scene)
	{
		/*
		 *		PURPOSE:
		 *				prepares rays and sends them
		 *			getting result points and sends them to shading
		 *
		 */
		Camera* camera = scene.cameraInScene;
		ImgPlane* screen = camera->screen;		//$NOFORGET: yeni bir screen yaratmiyor di mi?
		
		int nx = screen-> horRes;
		int ny = screen-> verRes;
		double perHorSize = ((screen-> right) - (screen-> left))/nx;
		double perVerSize = ((screen-> top) - (screen-> bottom))/ny;
		double left = screen-> left;
		double bottom = screen-> bottom;
		double uPt,vPt;
		double distance = screen-> distance;
		Ray ray;
		ray.origin = *(camera-> position);
		int i,j;
		Vector vecU = *(camera-> u);
		Vector vecV = *(camera-> v);
		Vector vecW = *(camera-> w);
		
		
		for(i=0 ; i<ny ; ++i)
		{
			for(j=0 ; j<nx ; ++j)
			{
				// for each pixel

				uPt = left + perHorSize*(j+0.5);
				vPt = bottom + perVerSize*(i+0.5);
				
				Vector _v( vecU.multiply(uPt) + vecV.multiply(vPt) + vecW.multiply(-distance) );
				ray.direction = &_v;
				
				Hit* h = checkIntersection(ray, scene);
				
				if( h )
				{
					shading(h, scene)
					
					delete h; //cout<< "@@@traceRay found hit in"<<h->p->x<<","<<h->p->y<<","<<h->p->z<<endl;
				// do sth w pObj
				//TODO: check color, shading, shadow etc
				}
			}
		}
	}
}
