#include <iostream>

#include "sceneStructure.h"

using std::cout;
using std::endl;

void printVector(Vector& v, int index=0)
{
	if( !index )
		cout<<v.x<<", "<<v.y<<", "<<v.z<<endl;
	else
		cout<<"index"<<index<<": "<<v.x<<", "<<v.y<<", "<<v.z<<endl;
}

int main(int argc, char* argv[])
{
	
	Vector v1;
	Vector v2( 1,1,1 );
	Vector v3( v2 );
	Point p(0.0,0.0,500.0);
	Vector v4( &p );
	Vector v5( &v4 );
	
	printVector(v1,1);
	printVector(v2,2);
	printVector(v3,3);
	printVector(v4,4);
	printVector(v5,5);

	v2.unitify();
	cout<<"--unitify---"<<endl;
	printVector(v2,1);
	
	cout<<"--isUnit?--"<<endl;
	if (v2.isUnit() )
		cout<<"yes v2 is unit!"<<endl;
	else
		cout<<"nooo something went wrong on v2"<<endl;
	if (v3.isUnit())
		cout<<"nooo somethingz wrong on v3"<<endl;
	else
		cout<<"yes v3 is not unit"<<endl;
	
	
	cout<<endl;

	v1 = Vector(1,0,0);
	v2 = Vector(0,1,0);
	v3 = v1.cross(&v2);
	
	cout<< "cross of 2 vectors below is third vector:"<<endl;
	printVector(v1);
	printVector(v2);
	printVector(v3);
	return 0;
}
