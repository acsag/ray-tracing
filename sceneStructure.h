#ifndef __scenestructure_h__
#define __scenestructure_h__
#include <iostream>
#include <vector>
#include <math.h>

using std::cout;
using std::endl;
using std::vector;

class Point
{
	public:
		double x;
		double y;
		double z;
		
		Point() {}
		Point( double xx, double yy, double zz ): x(xx), y(yy), z(zz) {}
		Point( float lst[3] ) : x(lst[0]), y(lst[1]), z(lst[2]) {}
		
		Point multiply(double d);  //TODO: test
		Point operator-(const Point& p);
		Point operator+(const Point& p);
		
		//Vector minusAsVector(Point* p);  //TODO: TEST
		//Vector* operator-(const Point* _p);
		// TODO: == , = vb seyleri dene sknti var mi?
};
class Vector
{
	public:
		double x;
		double y;
		double z;
		bool isUnit();
		void unitify();
		Vector multiply(double d); //TODO: TEST
		void negate();
		Point multiplyAsPoint(double m);  //yada + operatorunu overload? TODO:$DEL?
		// void multiply(double m); 		//multiplies as scalar, and changes the vector! other operators dont change just return new Vector! TODO
		double dot( Vector* v );	//NOFORGET: REFERENCE islerini yapamiyorum!
		Vector* cross(Vector* v );  //allocates new vector, never releases it!
		
		Vector() : x(-1.0), y(-1.0), z(-1.0) {}
		Vector(const Point& pp ) : x(pp.x), y(pp.y), z(pp.z) {}
		Vector( Point* pp ) : x(pp->x), y(pp->y), z(pp->z) {}
		Vector( Vector* pp ) : x(pp->x), y(pp->y), z(pp->z) {} 
		Vector( double xx, double yy, double zz ) : x(xx), y(yy), z(zz) {}
		//Vector( float lst[3] ) : x((double)lst[0]), y((double)lst[1]), z((double)lst[2]) {}
		Vector( float *lst ) : x((double)lst[0]), y((double)lst[1]), z((double)lst[2]) {}
		Vector operator-(const Vector& v);
		Vector operator+(const Vector& v);
		void printVector(); //for debug
};
class Vertex
{
	public:
		Point *_p;
		int _vertexID;
		Vertex(Point *point, int id)
		{
			_p = point;
			_vertexID = id;
		}
		~Vertex() {}//NO DESTRUCTOR! this class' job is until prepareScene()
		// AND no deallocation of Point too, since we use them in others
};
class Light
{
	public:
		double phongExp; 
		double r;
		double g;
		double b;
		Light() : r(-1), g(-1), b(-1) {}
		Light(float lst[3], float phong=1)
		{
			r = lst[0];
			g = lst[1];
			b = lst[2];
			phongExp = phong;
		}
	 	Light operator-(const Light& l);
	 	Light operator+(const Light& l);
};
class LightSource
{
	public:
		int _id;
		Point *position;
		Light *intensity;
		LightSource( Point *p, Light *l, int id )
		{
			position = p;
			intensity = l;
			_id = id;
		}
		~LightSource();
};
class Material
{
	public:
		int _id;
		int matID;
		Light *ambient;
		Light *diffuse;
		Light *specular;
		Light *reflectance;
		Material( Light *amb, Light *dif, Light *spec, Light *refl, int id )
		{
			ambient = amb;
			diffuse = dif;
			specular = spec;
			reflectance = refl;
			_id = id;
		}
		~Material();
};
struct Ray //class Ray
{
		Vector* direction;
		int reflectionCount;
		Point origin;
		bool reflection; 	// to determine whether it is reflection or sent from camera
};
struct ImgPlane //class ImgPlane
{
	//public:
		double left, right, bottom, top;
		double distance;		// distance to camera
		int horRes, verRes;		//resolution
		ImgPlane(double l, double r, double b, double t, double d, int nx, int ny)
		{
			left = l;
			right = r;
			bottom = b;
			top = t;
			distance = d;
			horRes = nx;
			verRes = ny;
		}
};
class Camera
{
	public:
		int _id;
		Point* position;
		Vector *u, *v, *w; // up (v) , 	gaze ( w ) , 	u ( yana )
		ImgPlane *screen;
		Camera(Point* p, Vector* vv, Vector* ww, ImgPlane* i, int id) //TODO: optimize structure!
		{
			this-> position = p;
			this-> v 		= vv;
			ww-> negate();		// reverses gaze direction
			this-> w		= ww;
			this-> screen 	= i;
			//this->u=NULL; $DEL $NOFORGET: use plain old 0 instead of NULL!!
			this-> u = v->cross(w);
			_id = id;
		}
		~Camera()
		{
			if (this->position)
				delete this-> position;
			this-> position = 0;
			if (this->u)
				delete this-> u;
			this-> u = 0;
			if (this->v)
				delete this-> v;
			this-> v = 0;
			if (this->w)
				delete this-> w;
			this-> w = 0;
			if (this->screen)
				delete this-> screen;
			this-> screen = 0;
		}
};
struct Hit
{
		Point *p;
		double t;		// defines t value in ray
	Hit()
	{
		this-> p = 0;
		t = 0;
	}
	~Hit()
	{
		if( this-> p )
			delete this-> p;
	}
};
class SceneObject
{
	public:
		int _matID;
	
	//	SceneObject() {};
	//	SceneObject( Material mtrl ){};
	
		//virtual bool intersect( const Ray& r, Hit* hit );
		virtual ~SceneObject() {}	 //no need to force pure abstraction, it can be used as instance
		int getID() { return _id; }
		void setID(int i) { _id = i; }
	
	protected:
		int _id;
};
class Sphere: public SceneObject
{
	public:
		int _vertex;

		Material *material;
		Point *center;
		double _radius;
		//Vector *normal;		//must be released!
		Vector getNormal(Point* p);

		bool intersect(Ray& ray, Hit*& h);
		Sphere(int vertex, int matID, float rad, int id)
		{
			_radius = (double)rad;
			_vertex = vertex;
			_matID = matID;
			_id = id;
			normal = 0;
			center = 0;
		}
		~Sphere();
};
class Triangle: public SceneObject
{
	public:
		int _vertices[3];
		Vector* normal;
		Vector* ab;		// from a to b ==>> (b-a)
		Vector* bc;
		Vector* ca;

		Material *material;
		Point *a, *b, *c;
		Vector getNormal();
		bool intersect(Ray& ray, Hit*& h);
		Triangle(int m, int lst[], int tID);
		~Triangle();
		
	private:

		void calcNormal();
		bool checkSamePlane(Ray& ray, Hit*& h);
		bool checkPointInside(Ray& ray, Hit*& h);
};
struct TempScene
{
		vector<Triangle*> triangleList;
		vector<Sphere*> sphereList;
};
class Scene
{
	public:
		// for parseSceneXML()
		TempScene* tempScene;
		vector<LightSource*> lightSourceList;
		vector<Vertex*> vertexList;
		vector<Material*> materialList;
		
		//after RYTRCR::prepScene()
		Triangle **triangleInScene;
		int triangleCount;
		Sphere **sphereInScene;
		int sphereCount;
		//Vertex **vertexInScene;
		//Material *materialInScene; // herkesin related material'i var, silerken once kontrol! TODO
		
		Light ambientInScene;
		Light backgroundInScene;
		int rayReflectCountInScene;
		Camera *cameraInScene;		// TODO: multiple camera??
		
		Scene()
		{
			tempScene = new TempScene();
			triangleCount = 0;
			sphereCount = 0;
			rayReflectCountInScene = 0;
			triangleInScene = 0;
			sphereInScene = 0;
		}
		~Scene();
};
/*
	
	LightSource class
		Light 

	Vector class
		attr:
			x,y,z
		method:
			bool isUnit()
			double dot(v1,v2)
			Vector crossProd(v1,v2)

	Ray class
		attr:
			Vector direction
			Point startingPt
			bool reflection /// for detecting reflection from obj or sent from camera!

		method:
			bool isReflection()
	
	Camera class extends SceneObject
		attr:
			Point location
			Vector up, gaze, .. //TODO: 3.yu biz hesapla!
			Screen coords!!		//TODO#1
				left,right,top,bottom //and other screen coords

	Scene class
		attr:
			Light background
			Material[]
			SceneObject[] //Sphere(vertex), Triangle
			Camera[] 				//TODO#1: screen coord icinde!
			LightSource lights[]




*/


#endif
