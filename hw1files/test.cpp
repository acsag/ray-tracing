#include <iostream>
#include "parseScene.h"
#include "writePPM.h"

int main(int argc, char* argv[])
{
    //
    // Test XML parsing
    //
    bool result = parseSceneXML(argv[1]);

    if (result)
    {
        std::cout << "Scene file parse successfully" << std::endl; 
    }
    else
    {
        std::cout << "ERROR parsing the scene file" << std::endl; 
    }

    //
    // Test PPM write
    //
    float data[] = {
        255, 0, 0,       0, 255, 0,    0, 0, 255,
        255, 255, 0,   255, 0, 255,    0, 255, 255,
    };

    writePPM("test.ppm", 3, 2, data);

    return 0;
}
