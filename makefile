CC = g++
CFLAGS = -c
DEBUG = -g

PTEST = testPoints.cpp
VTEST = testVectors.cpp
MTEST = testMaterial.cpp

TESTS = $(MTEST)


SOURCES = 	tinyXML/tinystr.cpp \
			tinyXML/tinyxml.cpp \
			tinyXML/tinyxmlerror.cpp \
			tinyXML/tinyxmlparser.cpp \
			sceneStructure.cpp \
			parseScene.cpp \
			raytracer.cpp \
			testIntersect.cpp #testPrepScene.cpp

OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = raymalifalitiko


#all: $(SOUCES) $(EXECUTABLE)

.cpp.o:
	$(CC) $(CFLAGS) $<

test: $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXECUTABLE)

debug: $(OBJECTS)
	$(CC) $(OBJECTS) $(DEBUG) -o $(EXECUTABLE)
clean:
	rm -f *.o *.out $(EXECUTABLE)
