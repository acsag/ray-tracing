#include <iostream>

#include "parseScene.h"
#include "raytracer.h"

int main(int argc, char* argv[])
{
	Scene scene;
	bool result = parseSceneXML(argv[1], &scene);

	if( result )
	{
		RYTRCR::prepareScene(scene);
		std::cout<<"! success"<<std::endl;
	}
	else
	{
		std::cout<<"!!!!!! try harder"<<std::endl;
	}
	return 0;
}
