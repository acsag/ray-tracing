#ifndef __raytracer_h__
#define __raytracer_h__

#include "sceneStructure.h"

namespace RYTRCR
{
	void prepareScene(Scene& scene);
	Hit* checkIntersection(Ray& ray, Scene& scene);
	Light shadingDiffuse(Hit& h, Scene& scene);
	Light shadingSpecular(Hit& h, Scene& scene);
	Light shadingAmbient(Scene& scene);
	Light shading(Hit& h, Scene& s);
	void traceRays(Scene& scene);

}
#endif
