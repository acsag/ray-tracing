#include <iostream>


 //NULL DEFINITION
static const int nullptr = 0;

using namespace std;
/*
class A {
	public:
	virtual void gett(){
		cout<< "This is the A"<<endl;
	}
};

class BA : public A {
	public:
	virtual void gett(){
		cout<<"this is the B!"<<endl;
	}
};

class CA: public A  {
	public:
		virtual void gett(){
			cout<<"This IS THE C"<<endl;
		}
};

class D: public A {
};


int main()
{	
	A* lst = new A[4];
	lst[0] = new A();
	lst[1] = new B();
	lst[2] = new C();
	lst[3] = new D();
	
	for(int i=0; i<4; ++i)
		lst[i].gett();


	return 0;
}

*/
class Base
{
  public:
	  void Method1 ()  {  std::cout << "Base::Method1" << std::endl;  }
		  virtual void Method2 ()  {  std::cout << "Base::Method2" << std::endl;  }
		  };

class Derived : public Base
			  {
	public:
		void Method1 ()  {  std::cout << "Derived::Method1" << std::endl;  }
		void Method2 ()  {  std::cout << "Derived::Method2" << std::endl;  }
					};
int main()
{
/*
	Base* obj = new Derived ();
				  //  Note - constructed as Derived, but pointer stored as Base*

	obj->Method1 ();  //  Prints "Base::Method1"
	obj->Method2 ();  //  Prints "Derived::Method2"
*/
	Base** lst=new *(Base[4]);
	lst[0] = new Derived();
	
	for(int i=0;i<4;++i)
	{
	//	if(  lst[i] == nullptr )
	//		continue;
		lst[i]->Method1();
		lst[i]->Method2();
	}

	delete lst;
	return 0;
}
