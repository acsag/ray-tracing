#include <iostream>

#include "sceneStructure.h"

using std::cout;
using std::endl;

void printLight(Material* m)
{
	Light * v = m->ambient;
		cout<<v->r<<", "<<v->g<<", "<<v->b<<endl;
}

void delLight(Light* l)
{
	cout<<"inside delLight"<<(void*)l<<endl;
	if( l != 0 )
		delete l;
	l = 0;
	cout<<"inside delLight"<<(void*)l<<endl;
}
void delMaterial(Material* m)
{
	cout<<"inside delMat"<<(void*)m<<endl;
	if( m != 0 )
		delete m;
	m = 0;
	cout<<"inside delMat"<<(void*)m<<endl;
}

int main(int argc, char* argv[])
{
	float arr1[] = {10.0,10.2,199.1};
	float arr2[] = {100.8,200.9,300.0};
	Light* l1 = new Light();
	Light* l2 = new Light();
	Light* l3 = new Light(arr1);
	Light* l4 = new Light(arr2);
	Light* l5 = new Light(*l3);
	Light* l6 = new Light();
	Light* l7 = new Light();
	Light* l8 = new Light();
	Material* m1 = new Material(l1,l2,l3,l4,1);
	Material* m2 = new Material(l1,l2,l3,l4,2);
	
	delLight(l1);
	cout<<"l1"<<(void*)l1<<endl;
	delLight(l2);
	delLight(l3);
	delLight(l4);
	delLight(l5);
	delLight(l6);
	delLight(l7);
	delLight(l8);
	delMaterial(m1);
	cout<<"m1"<<(void*)m1<<endl;
	delMaterial(m2);

	return 0;
}
